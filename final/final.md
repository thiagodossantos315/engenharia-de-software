2. Se você fosse iniciar um projeto para desenvolver um software em equipe, qual processo de software você usaria? Explique este processo, indicando uma vantagem e uma desvantagem de utilizá-lo.

3. Qual a utilidade de um modelo de processo de software em um projeto de desenvolvimento de software?

4. Vamos considerar que estimamos as histórias de usuário de nosso projeto em pontos, de forma que 1 ponto equivale a 8 horas de trabalho (1 dia ideal). Estime quantos pontos consegue entregar em:
Uma Sprint de 4 semanas, com 6 horas diárias de trabalho.

5.Escolha uma prática da Extreme Programming (XP) e explique como ela é realizada.

6. Quando há uma mudança no projeto, como isso é implementado com o Gantt? Quando há uma mudança no projeto, como isso é implementado com o Kanban?

7. Explique o que significa "Limitar o WIP" em um Kanban.

8. Explique o que significa "Limitar o WIP" em um Kanban.

9. Se você fosse o gerente de projetos para desenvolver um software e pudesse escolher somente uma área de conhecimento do PMBOK para implementar em seu projeto. Qual você escolheria? Explique esta área de conhecimento e porquê você a escolheu.

10. Se você fosse o gerente de projetos para desenvolver um software e pudesse escolher somente uma área de conhecimento do PMBOK para implementar em seu projeto. Qual você escolheria? Explique esta área de conhecimento e porquê você a escolheu.

11. Cite e explique cada uma das fases do Processo Unificado.

12. Qual a diferença entre esforço e duração de uma atividade? Explique e dê um exemplo de como calcular a duração com base no esforço.

13. Qual a diferença entre esforço e duração de uma atividade? Explique e dê um exemplo de como calcular a duração com base no esforço.

